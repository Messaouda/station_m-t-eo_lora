#include <ArduinoJson.h>

//Exemples (pour TBEAM)
// ESP32 -> Wifi -> Wificlientbasic
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <DHT_U.h>
#include <DHT.h>
#define DHTPIN 15
#define DHTTYPE DHT11
#include<ArduinoJson.h>
StaticJsonDocument <1500>forecast;
StaticJsonDocument <200>actions;
#include "config.h"
int d = 1000;
WiFiMulti WiFiMulti;
void led (int numled) {
  
  digitalWrite(numled, HIGH);
  delay(200);
}
DHT sensor(DHTPIN, DHTTYPE);
void setup()
{
  pinMode(13, OUTPUT);
  pinMode(14, OUTPUT);
  //Openning Serial Port @ 115200
  Serial.begin(115200);
  sensor.begin();//init sensor
  // Waiting 10 ms (WHY??????)
  // Connecting to The AP
  WiFiMulti.addAP(SSID, PASSWORD);
  // While the wifi is note connected, we retry and wait 500 ms
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
  }
  //Display the Ip adress if it's connected.
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  // We Are Ready !!
}
void send_data() {
HTTPClient http;

  delay(d);
  float h = sensor.readHumidity();
  float t = sensor.readTemperature();
  String host = "10.130.1.237:1880";
  //Url to Contact/Connect
  String url = "/sensor/g09?temp=" + String (t) + "&hygro=" + String (h);
  http.begin("http://" + host + url);
  int retCode = http.GET();
  Serial.print(" [HTTP] ""Return Code = ");
  Serial.println(retCode);
  if (retCode == HTTP_CODE_OK) {
    String content = http.getString();
    Serial.println(content);
    led(13);
  }
  else {
    // An Error occured ....
    Serial.print ("[HTTP] GET failed, error : ");
    Serial.println(retCode);
    led(14);
  }
  Serial.print("Humidité :");
  Serial.println(h);
  Serial.print("Température : ");
  Serial.println(t);
  http.end();
  
}


void get_actions() {
  HTTPClient http;
delay(d);
  String host = "10.130.1.237:1880";
  //Url to Contact/Connect
  String url = "/getactions";
  http.begin("http://" + host + url);
  int retCode = http.GET();
  Serial.print(" [HTTP] ""Return Code = ");
  Serial.println(retCode);
  if (retCode == HTTP_CODE_OK) {
    String content = http.getString();
    Serial.println(content);
    DeserializationError error = deserializeJson(actions, content);
    // Test if parsing succeeds.
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      return;
      
    }//Endif parse ERROR
    //Parsing was ok, grabbing values:

    if (actions["porte"]==1){
      Serial.println("OUVERTURE PORTE...");
    }
    
    led(13);
  } 

  //Parsingwasok, grabbing values:

  else {
    // An Error occured ....
    Serial.print ("[HTTP] GET failed, error : ");
    Serial.println(retCode);
    led(14);
  }

  http.end();
}


void get_data() {
  HTTPClient http;

  delay(d);
  String host = "api.openweathermap.org";
  //Url to Contact/Connect
  String url = "/data/2.5/forecast?q=willems,fr&appid=" + String(APIKEY)+ "&cnt=1";
  http.begin("http://" + host + url);
  int retCode = http.GET();
  Serial.print(" [HTTP] ""Return Code = ");
  Serial.println(retCode);
  if (retCode == HTTP_CODE_OK) {
    String content = http.getString();
    Serial.println(content);
    DeserializationError error = deserializeJson(forecast, content);
    // Test if parsing succeeds.
    if (error) {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.c_str());
      return;
      
    }//Endif parse ERROR
    float Wspeed=forecast["list"][0]["wind"]["speed"];
      float Wdirection=forecast["list"][0]["wind"]["deg"];
      Serial.print("Wind speed (m/s)=");
      Serial.println(Wspeed);
      Serial.print("Wind direction (degrés)=");
      Serial.println(Wdirection);
    
    led(13);
  } 

  //Parsingwasok, grabbing values:

  else {
    // An Error occured ....
    Serial.print ("[HTTP] GET failed, error : ");
    Serial.println(retCode);
    led(14);
  }

  http.end();
}



void loop()
{
  //Serial.println("========= send data =======");
  send_data();
  //Serial.println("======= get data =====");
  //get_data();
  get_actions();
  
  delay(5000);
}
